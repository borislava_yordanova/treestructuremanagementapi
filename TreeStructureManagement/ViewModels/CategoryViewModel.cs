﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Web;
using TreeStructureManagement.Models;

namespace TreeStructureManagement.ViewModels
{
    public class CategoryViewModel
    {
        [Display(Name = "Category Id")]
        public int Id { get; set; }

        [Required(ErrorMessage = "Please enter Name.")]
        [Display(Name = "Name")]
        public string Name { get; set; }

        [Display(Name = "Parent Id")]
        public int Pid { get; set; }

        [Required(ErrorMessage = "Please enter a description.")]
        [Display(Name = "Description")]
        public string Description { get; set; }

        [ForeignKey("Pid")]
        public virtual Category Parent { get; set; }
        public virtual ICollection<Category> Childs { get; set; }
    }
}