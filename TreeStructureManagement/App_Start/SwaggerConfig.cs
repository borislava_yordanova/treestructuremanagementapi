using System.Web.Http;
using WebActivatorEx;
using TreeStructureManagement;
using Swashbuckle.Application;

[assembly: PreApplicationStartMethod(typeof(SwaggerConfig), "Register")]

namespace TreeStructureManagement
{
    public class SwaggerConfig
    {
        public static void Register()
        {
            var thisAssembly = typeof(SwaggerConfig).Assembly;

            GlobalConfiguration.Configuration
                .EnableSwagger(c => c.SingleApiVersion("v1", "Tree Structure Management"))
                .EnableSwaggerUi();
                   
        }
    }
}
