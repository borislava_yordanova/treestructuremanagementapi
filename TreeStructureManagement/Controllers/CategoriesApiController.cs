﻿using AutoMapper;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Threading.Tasks;
using System.Web.Http;
using System.Web.Http.Description;
using TreeStructureManagement.Models;
using TreeStructureManagement.Repositories;
using TreeStructureManagement.ViewModels;

namespace TreeStructureManagement.Controllers
{
    public class CategoriesApiController : ApiController
    {
        private IBaseRepository<Category> _repository = null;

        public CategoriesApiController()
        {
            this._repository = new BaseRepository<Category>();
        }

        [Route("api/categories")]
        [HttpGet]
        public async Task<IEnumerable<Category>> Get()
        {
            var categories = _repository.SelectAll().ToList();
            var categoriesViewModel = Mapper.Map<IEnumerable<CategoryViewModel>>(categories);

            await Task.Yield();
            return _repository.SelectAll();
        }

        [Route("api/categories/{id}")]
        [HttpGet]
        public async Task<Category> Get(int? id)
        {
            var category = _repository.SelectByID(id.Value);
            var categoryViewModel = Mapper.Map<CategoryViewModel>(category);

            await Task.Yield();
            return _repository.SelectByID(id.Value);
        }

        [Route("api/categories/search")]
        [HttpGet]
        [ResponseType(typeof(List<CategoryViewModel>))]
        public async Task<IEnumerable<CategoryViewModel>> Search([FromUri]string search)
        {
            var categories = _repository.SelectAll().ToList();
            var categoryViewModel = Mapper.Map<IEnumerable<CategoryViewModel>>(categories);

            if (!String.IsNullOrEmpty(search))
            {
                categoryViewModel = categoryViewModel.Where(s => s.Name.Contains(search));
            }
            await Task.Yield();
            return categoryViewModel;
        }

        [Route("api/categories")]
        [ResponseType(typeof(Category))]
        public HttpResponseMessage Post(CategoryViewModel categoryViewModel)
        {
            var category = Mapper.Map<Category>(categoryViewModel);
            _repository.Insert(category);
            _repository.Save();

            return Request.CreateResponse(HttpStatusCode.Created, category);
        }

        [Route("api/categories/{id}")]
        [ResponseType(typeof(Category))]
        public HttpResponseMessage Put([FromBody]CategoryViewModel categoryViewModel)
        {
            List<Category> categories = _repository.SelectAll().ToList();

            var existingCategory = categories.FirstOrDefault(c => c.Id == categoryViewModel.Id);
            if (existingCategory == null)
                return Request.CreateErrorResponse(HttpStatusCode.NotFound, "Category not found");

            var category = Mapper.Map<Category>(categoryViewModel);
            _repository.Delete(existingCategory);
            _repository.Insert(category);
            _repository.Save();

            return Request.CreateResponse(HttpStatusCode.OK, category);
        }

        [Route("api/categories/{id}")]
        [HttpDelete]
        public HttpResponseMessage Delete(int? id)
        {
            var category = _repository.SelectByID(id.Value);
            if (category == null)
                return Request.CreateErrorResponse(HttpStatusCode.NotFound, "Category not found");

            _repository.Delete(category);
            _repository.Save();

            return Request.CreateResponse(HttpStatusCode.OK);
        }
    }
}
