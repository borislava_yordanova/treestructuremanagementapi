﻿using AutoMapper;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Web.Mvc;
using TreeStructureManagement.Models;
using TreeStructureManagement.Repositories;
using TreeStructureManagement.ViewModels;

namespace TreeStructureManagement.Controllers
{
    public class CategoriesController : Controller
    { 
        private IBaseRepository<Category> _repository = null;

        public CategoriesController()
        {
            this._repository = new BaseRepository<Category>();
        }
      
        public async Task<ActionResult> Index(string searchString)
        {
            var categories = _repository.SelectAll().ToList();
            var categoryViewModel = Mapper.Map<IEnumerable<CategoryViewModel>>(categories);

            if (!String.IsNullOrEmpty(searchString))
            {
                categoryViewModel = categoryViewModel.Where(s => s.Name.Contains(searchString));
            }
            await Task.Yield();
            return View(categoryViewModel);
        }

        public async Task<ActionResult> Details(int? id)
        {
            var category = _repository.SelectByID(id.Value);
            var categoryViewModel = Mapper.Map<CategoryViewModel>(category);

            await Task.Yield();
            return View(categoryViewModel);
        }

        public async Task<ActionResult> Create()
        {
            var newCategory = new CategoryViewModel();

            await Task.Yield();
            return View(newCategory);
        }

        [HttpPost]
        public async Task<ActionResult> Create(CategoryViewModel categoryViewModel)
        {
            var category = new Category();
           
            var newCategory = Mapper.Map(categoryViewModel, category);
            _repository.Insert(newCategory);
            _repository.Save();

            await Task.Yield();
            return RedirectToAction("Index");
        }

        public async Task<ActionResult> Edit(int? id)
        {
            var category = _repository.SelectByID(id.Value);
            var viewModel =  new CategoryViewModel();
            var newCategory = Mapper.Map(category, viewModel);

            await Task.Yield();
            return View(newCategory);
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> Edit(int? id, CategoryViewModel categoryViewModel)
        {          
            var category = _repository.SelectByID(id.Value);
            var newCategory = Mapper.Map(categoryViewModel, category);
            _repository.Update(category);
            _repository.Save();

            await Task.Yield();
            return RedirectToAction("Index");
        }

        public async Task<ActionResult> Delete()
        { 
            var deletedCategory = new CategoryViewModel();

            await Task.Yield();
            return View(deletedCategory);
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Delete(CategoryViewModel categoryViewModel, int? id)
        {      
            var category = _repository.SelectByID(id.Value);
            var deletedCategory = Mapper.Map(categoryViewModel, category);
            _repository.Delete(deletedCategory);
            _repository.Save();


            return RedirectToAction("Index");
        }
    }
}