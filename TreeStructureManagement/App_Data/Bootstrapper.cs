﻿using TreeStructureManagement.Mappings;

namespace TreeStructureManagement.App_Start
{
    public class Bootstraper
    {
        public static void Run()
        {
            AutoMapperConfiguration.Configure();
        }
    }
}